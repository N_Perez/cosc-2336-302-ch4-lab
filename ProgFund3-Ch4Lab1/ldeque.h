/*
Nicholas Perez COSC 2336-302
Chapter 4 Lab 1 - Lists Lab
Project 4.1
*/
#include "link.h"



template <typename E> class LDeck{
private:
  Link<E>* front;       // Pointer to front queue node
  Link<E>* rear;        // Pointer to rear queue node
  int size;                // Number of elements in queue

public:
  LDeck() // Constructor 
    {
		front = rear = new Link<E>(); // Creates a new node, sets front and rear pointers to point at it
		size = 0;
	}

  ~LDeck() { clear(); delete front; }      // Destructor

  void clear() 
  {           // Clear queue
    while(front->next != NULL) // Delete each link node 
	{ 
		Link<E>* ltemp = front->next; // Hold soon-to-be deleted link
		front->next = ltemp->next;       // Advance front
		if (rear == ltemp) 
			{
				rear = front; // about to delete last link
				front->next=NULL; 
			}
		delete ltemp;                    // Delete link
    }
    size = 0;
  }

  void enqueue(const E& it) // Put element on rear
  { 
    rear->next = new Link<E>(it, NULL, NULL); //Sets the rear 'next' pointer to point to a new link
	Link<E>* temp = rear; //creates a temporary pointer that indicates what is now 2nd from rear
    rear = rear->next; //sets the 'rear' pointer to point to the new link
	rear->prev= temp; // sets the new link's previous pointer to the one before it
    size++; 
  }

  E dequeue() // Remove element from front
  {
    //assert(size != 0, "Queue is empty"); //how to into asserts
	  if (size == 0)
	  {
		  cout << "Deque is empty." << endl;
		  abort();
	  }

	  else
	  {
		E it = front->next->element;  // Store dequeued value
		Link<E>* ltemp = front->next; // Hold dequeued link
		front->next = ltemp->next;       // Advance front
		if (rear == ltemp) 
			{
				rear = front; // Dequeue last element
				front->next=NULL; 
			}
		delete ltemp;                    // Delete link
		size --;
		return it;                       // Return element value
	  }
  }

  //deque functions, but I'll be using "deck" throughout 

  void endeck(const E& it) // Put element on front
  { 
	  if (front->next == NULL) // No elements in the deck
	  {
		  front->next = new Link<E>(it, NULL, NULL); //Creates a new link, sets the front's 'next' pointer to it
		  front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
		  rear = front->next;
	  }
	  else
	  {
		  front->next->prev = new Link<E>(it, NULL, NULL); //Sets the first element's 'previous' pointer to point to a new link
		  Link<E>* temp = front->next; //creates a temporary pointer that indicates what is going to be 2nd from front
		  front->next = temp->prev; // Sets the front's 'next' pointer to point to the new link
		  front->next->next = temp; // Sets the new link's 'next' pointer to point to what is now the second element from front
		  front->next->prev = front; //Sets the new link's 'prev' pointer to point to the front link
	  }
    
	
    size++; 
  }

  E dedeck() // Remove element from back
  {
    //assert(size != 0, "Queue is empty"); 
	  if (size == 0)
	  {
		  cout << "Deque is empty." << endl;
		  abort();
	  }

	  else
	  {
		E it = rear->element;  // Store dedecked value
		Link<E>* ltemp = rear; // Hold dedecked link
		rear = ltemp->prev;       // Advance rear
		
		if (front->next == ltemp) 
			{
				front->next = NULL; // Dedecked last element, empty the front's next pointer 
				rear = front;
			}
			
		delete ltemp;                    // Delete link
		size --;
		return it;                       // Return element value
	  }
  }


  //testing functions

  const E& frontValue() const 
  { // Get front element	  
    //assert(size != 0, "Queue is empty");
	  if (size == 0)
	  {
		  cout << "Deque is empty." << endl;
		  abort();
	  }
	  else
	  {
		return front->next->element;
	  }
  }
  const E& rearValue() const 
  { // Get rear element	  
    //assert(size != 0, "Queue is empty");
	  if (size == 0)
	  {
		  cout << "Deque is empty." << endl;
		  abort();
	  }
	  else
	  {
		return rear->element;
	  }
  }

  virtual int length() const 
  {
	return size;
  }

};
