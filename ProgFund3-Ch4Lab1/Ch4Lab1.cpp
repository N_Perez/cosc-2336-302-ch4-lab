/*
Nicholas Perez COSC 2336-302
Chapter 4 Lab 1 - Lists Lab
Project 4.1
*/

#include "ldeque.h"

#include <iostream>


using namespace std;


int main()
{
	LDeck<int> van;
	
	cout << "Putting some numbers in the deck..." << endl;
	cout << "Queue up 4, first in line!" <<endl;
	van.endeck(4);
	cout << "Putting 3 at the front!" <<endl;
	van.endeck(3);
	cout << "Putting 2 at the front!" <<endl;
	van.endeck(2);
	cout << "Putting 1 at the front!" <<endl;
	van.endeck(1);

	cout << "Size of deck is: " << van.length() << endl;
	cout << "First value is: " << van.frontValue() << endl;
	cout << "Rear value is: " << van.rearValue() << endl;

	cout << "Dequeueing, front to rear: " <<endl;
	cout << "First link is: " << van.dequeue() << endl;
	cout << "Second Link is: " << van.dequeue() << endl;
	cout << "Third link is: " << van.dequeue() << endl;
	cout << "Fourth link is: " << van.dequeue() << endl;	
		cout << "Size of deck is now: " << van.length() << endl<< endl;

	cout << "Putting some numbers in the deck..." << endl;
	cout << "Queue up 2, first in line!" <<endl;
	van.enqueue(2);
	cout << "Queue up 4 at the rear!" <<endl;
	van.enqueue(4);
	cout << "Queue up 6 at the rear!" <<endl;
	van.enqueue(6);
	cout << "Queue up 8 at the rear!" <<endl;
	van.enqueue(8);
	
	cout << "Size of deck is: " << van.length() << endl;
	
	cout << "First value is: " << van.frontValue() << endl;
	cout << "Rear value is: " << van.rearValue() << endl;

	
	cout << "Dequeueing, rear to front: " <<endl;
	cout << "Fourth link is: " << van.dedeck() << endl;
	cout << "Third Link is: " << van.dedeck() << endl;
	cout << "Second link is: " << van.dedeck() << endl;
	cout << "First link is: " << van.dedeck() << endl;
	cout << "Size of deck is now: " << van.length() << endl<<endl;
	
	cout << "Putting some numbers in the deck..." << endl;
	cout << "Queue up 3, first in line!" <<endl;
	van.enqueue(3);
	cout << "Queue up 6 at the rear!" <<endl;
	van.enqueue(6);
	cout << "Queue up 9 at the rear!" <<endl;
	van.enqueue(9);
	cout << "Queue up 12 at the rear!" <<endl;
	van.enqueue(12);	
	
	cout << "Size of deck is: " << van.length() << endl;		
	cout << "First value is: " << van.frontValue() << endl;
	cout << "Rear value is: " << van.rearValue() << endl;
	
	
	cout << "Dequeueing, rear, front, rear, front: " <<endl;
	cout << "Fourth link is: " << van.dedeck() << endl;
	cout << "First link is: " << van.dequeue() << endl;	
	cout << "Third Link is: " << van.dedeck() << endl;
	cout << "Second Link is: " << van.dequeue() << endl;
	cout << "Size of deck is now: " << van.length() << endl<<endl;
	
	
	cout << "Putting some numbers in the deck..." << endl;
	cout << "Queue up 4, first in line!" <<endl;
	van.enqueue(4);
	cout << "Queue up 8 at the rear!" <<endl;
	van.enqueue(8);
	cout << "Queue up 12 at the rear!" <<endl;
	van.enqueue(12);
	cout << "Queue up 16 at the rear!" <<endl;
	van.enqueue(16);
	
	cout << "Size of deck is: " << van.length() << endl;		
	cout << "First value is: " << van.frontValue() << endl;
	cout << "Rear value is: " << van.rearValue() << endl;
	
	cout << "Dequeueing, front, rear, front, rear: " <<endl;
	cout << "First link is: " << van.dequeue() << endl;
	cout << "Fourth link is: " << van.dedeck() << endl;
	cout << "Second Link is: " << van.dequeue() << endl;
	cout << "Third Link is: " << van.dedeck() << endl;
	cout << "Size of deck is now: " << van.length() << endl<<endl;
	
	
	cout << "Putting some numbers in the deck..." << endl;
	cout << "Queue up 15, first in line!" << endl;
	van.enqueue(15);
	cout << "Putting 10 at the front!" << endl;
	van.endeck(10);
	cout << "Queue up 20 at the rear!" << endl;
	van.enqueue(20);
	cout << "Putting 5 at the front!" << endl;
	van.endeck(5);
	
	cout << "Size of deck is: " << van.length() << endl;
	cout << "First value is: " << van.frontValue() << endl;
	cout << "Rear value is: " << van.rearValue() << endl;
	
	cout << "Dequeueing, front to rear: " << endl;
	cout << "First link is: " << van.dequeue() << endl;
	cout << "Second Link is: " << van.dequeue() << endl;
	cout << "Third link is: " << van.dequeue() << endl;
	cout << "Fourth link is: " << van.dequeue() << endl;
	cout << "Size of deck is now: " << van.length() << endl << endl;
	
	/*
	//testing clearing
	cout << "Clearing the deck..." << endl;
	van.clear();
	*/
	system("pause");
	return 0;
}
