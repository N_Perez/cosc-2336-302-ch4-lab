/*
Nicholas Perez COSC 2336-302
Chapter 4 Lab 1 - Lists Lab
Project 4.1
*/
#define NULL 0
template <typename E> class Link {
public:
  E element;      // Value for this node
  Link *next;        // Pointer to next node in list
  Link *prev;        // Pointer to last node in list
  // Constructors
  Link(const E& elemval, Link* nextval =NULL, Link* prevval =NULL)
    {
		element = elemval;  
		next = nextval; 
		prev = prevval;
	}
  Link(Link* nextval =NULL, Link* prevval =NULL) 
	{
	  next = nextval; 
	  prev = prevval;
	}
};
